package tech.mastertech.itau.exemplos;

public class Estruturas {

	
	public boolean maiorQueCinco(int numero) {

	    int i = 0;
	    
	    int [] numeros = {1, 2, 3, 4, 5, 6};
	    
	    for(int valor : numeros) {
	    	
	    	System.out.println(valor * 3);
	    }
	    
		while(i < 10) {
			System.out.println("Contando pelo while:" + i);
			i++;
		}
		
		for(i = 0 ; i < 10 ; i++) {
			System.out.println("Contando pelo for:" + i);
			
		}
		
		if (numero > 5) {
			return true;
		}
		return false;
	}
	
	

}
